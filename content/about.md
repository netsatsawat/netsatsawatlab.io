---
author: "Satsawat Natakarnkitkul"
linktitle: About
title: About Me
menu: main
permalink: /about/
aliases:
  - "/cv/"
  - "/cv.html"
---

<p align="center">
  <img src=https://gitlab.com/netsatsawat/netsatsawat.gitlab.io/raw/master/content/images/about-profile.jpg width="180" height="280">
</p>


Satsawat is data enthusiast, data scientist and machine learning engineer, focused on building and delivering the value to business based on the data.

He is currently working for Knowesis Pte, a leading company in providing real time data analytics driving contextual marketing. He has worked at dtac, Accenture and IBM as senior data scientist, and analytic consultant.

He has a Master's degree from University of Exeter, UK, and Bachelor's degree from King Monkut's University of Technology Thonburi, Thailand.


### Skills

{{< labels Python R SAS "Data Science" Telecommunication "Business Consulting" Leadership AdTech MarTech "Contextual Marketing" >}}


